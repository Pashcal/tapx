﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.IsolatedStorage;
using System.Security.Permissions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;

namespace TapX
{
    public partial class MainPage : PhoneApplicationPage
    {
        private class xy
        {
            public int x { set; get; }
            public int y { set; get; }
            public bool isFree { set; get; }

            public xy(int X, int Y, bool IsFree)
            {
                x = X;
                y = Y;
                isFree = IsFree;
            }
        }

        private string color;
        private int Max = 84;
        private int mX = 8;
        private int mY = 11;
        private int score;
        private int best;
        private int count;
        private int win;
        private int t;
        private DispatcherTimer timer;
        private IsolatedStorageSettings setting;
        private List<xy> pos;
        private List<string> themes = new List<string>() { "system", "dark", "light" };

        public MainPage()
        {
            InitializeComponent();
            pos = new List<xy>();
            win = 0;
            count = 0;
            score = 0;
            best = 0;
            setting = IsolatedStorageSettings.ApplicationSettings;
            if (setting.Contains("Best")) best = (int) setting["Best"];
            //Theme.ItemsSource = themes;
            //if (setting.Contains("Theme")) Theme.SelectedIndex = (int) setting["Theme"];
            //else Theme.SelectedIndex = 0;
            Score.Text = score.ToString();
            Best.Text = best.ToString();
            timer = new DispatcherTimer();
            timer.Tick += new EventHandler(TimerTick);
            timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            AdRotator.Invalidate();
            UpdateColor();
        }

        private void UpdateColor()
        {
            Uri uri;
            switch (0)//Theme.SelectedIndex)
            {
                case 0:
                    Visibility darkBackgroundVisibility = (Visibility)Application.Current.Resources["PhoneDarkThemeVisibility"];
                    if (darkBackgroundVisibility == Visibility.Visible) color = "w";
                    else color = "b";
                    uri = new Uri(String.Format("Images/{0}/{1}.png", color, win), UriKind.Relative);
                    CorrectImg.Source = new BitmapImage(uri);
                    LayoutRoot.Background = Application.Current.Resources["PhoneBackgroundBrush"] as SolidColorBrush;
                    TAP.Foreground = Application.Current.Resources["PhoneForegroundBrush"] as SolidColorBrush;
                    TO_WIN.Foreground = Application.Current.Resources["PhoneForegroundBrush"] as SolidColorBrush;
                    Time.Foreground = Application.Current.Resources["PhoneForegroundBrush"] as SolidColorBrush;
                    Timer.Foreground = Application.Current.Resources["PhoneForegroundBrush"] as SolidColorBrush;
                    Score.Foreground = Application.Current.Resources["PhoneForegroundBrush"] as SolidColorBrush;
                    BestBorder.Background = Application.Current.Resources["PhoneForegroundBrush"] as SolidColorBrush;
                    Best.Foreground = Application.Current.Resources["PhoneBackgroundBrush"] as SolidColorBrush;
                    CenterTextBorder.Background = Application.Current.Resources["PhoneBackgroundBrush"] as SolidColorBrush;
                    CenterTextBlock.Foreground = Application.Current.Resources["PhoneForegroundBrush"] as SolidColorBrush;
                    LinkToOriginal.Foreground = Application.Current.Resources["PhoneForegroundBrush"] as SolidColorBrush;
                    AdRotatorBlock.Background = Application.Current.Resources["PhoneBackgroundBrush"] as SolidColorBrush;
                    break;
                case 1:
                    color = "w";
                    uri = new Uri(String.Format("Images/{0}/{1}.png", color, win), UriKind.Relative);
                    CorrectImg.Source = new BitmapImage(uri);
                    LayoutRoot.Background = new SolidColorBrush(Colors.Black);
                    TAP.Foreground = new SolidColorBrush(Colors.White);
                    TO_WIN.Foreground = new SolidColorBrush(Colors.White);
                    Time.Foreground = new SolidColorBrush(Colors.White);
                    Timer.Foreground = new SolidColorBrush(Colors.White);
                    Score.Foreground = new SolidColorBrush(Colors.White);
                    BestBorder.Background = new SolidColorBrush(Colors.White);
                    Best.Foreground = new SolidColorBrush(Colors.Black);
                    CenterTextBorder.Background = new SolidColorBrush(Colors.Black);
                    CenterTextBlock.Foreground = new SolidColorBrush(Colors.White);
                    LinkToOriginal.Foreground = new SolidColorBrush(Colors.White);
                    AdRotatorBlock.Background = new SolidColorBrush(Colors.Black);
                    break;
                case 2:
                    color = "b";
                    uri = new Uri(String.Format("Images/{0}/{1}.png", color, win), UriKind.Relative);
                    CorrectImg.Source = new BitmapImage(uri);
                    LayoutRoot.Background = new SolidColorBrush(Colors.White);
                    TAP.Foreground = new SolidColorBrush(Colors.Black);
                    TO_WIN.Foreground = new SolidColorBrush(Colors.Black);
                    Time.Foreground = new SolidColorBrush(Colors.Black);
                    Timer.Foreground = new SolidColorBrush(Colors.Black);
                    Score.Foreground = new SolidColorBrush(Colors.Black);
                    BestBorder.Background = new SolidColorBrush(Colors.Black);
                    Best.Foreground = new SolidColorBrush(Colors.White);
                    CenterTextBorder.Background = new SolidColorBrush(Colors.White);
                    CenterTextBlock.Foreground = new SolidColorBrush(Colors.Black);
                    LinkToOriginal.Foreground = new SolidColorBrush(Colors.Black);
                    AdRotatorBlock.Background = new SolidColorBrush(Colors.White);
                    break;
            }
            UpdatePos();
        }
        
        private void Theme_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateColor();
        }

        private void Next()
        {
            count += (count < Max) ? 1 : 0;
            Random r = new Random();
            win = r.Next(count);
            UpdatePos();
        }

        private void UpdatePos()
        {
            Panel.Children.Clear();
            pos.Clear();
            for (int i = 0; i < Panel.ColumnDefinitions.Count; i++)
                for (int j = 0; j < Panel.RowDefinitions.Count; j++)
                    pos.Add(new xy(i, j, true));

            Random r = new Random();
            for (int i = 0; i < count; i++)
            {
                xy p = pos[r.Next(Max)];
                while (!p.isFree) p = pos[r.Next(Max)];
                p.isFree = false;

                Uri uri = new Uri(String.Format("Images/{0}/{1}.png", color, i), UriKind.Relative);
                Image img = new Image();
                img.Source = new BitmapImage(uri);
                img.Stretch = Stretch.None;
                img.SetValue(Grid.ColumnProperty, p.x);
                img.SetValue(Grid.RowProperty, p.y);
                if (i == win)
                {
                    img.Tap += CorrectTap;
                    CorrectImg.Source = img.Source;
                }

                Panel.Children.Add(img);
            }
        }

        private void CorrectTap(object o, EventArgs a)
        {
            score += t;
            Score.Text = score.ToString();
            if (best < score)
            {
                best = score;
                Best.Text = best.ToString();
            }
            Next();
            t = 199;
        }

        private void GameOver()
        {
            timer.Stop();
            AdRotatorBlock.Visibility = Visibility.Visible;
            if (best < score) best = score;
            setting["Best"] = best;
            setting.Save();
            count = 0;
            score = 0;
            Panel.Children.Clear();
            CenterTextBlock.Text = "GAME OVER!\nretry?";
            CenterTextBorder.Visibility = Visibility.Visible;
            LinkToOriginal.Visibility = Visibility.Visible;
        }

        private void TimerTick(object o, EventArgs a)
        {
            if (t == 0) GameOver();
            Timer.Text = t--.ToString();
        }

        private void LayoutRoot_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (CenterTextBlock.Visibility == Visibility.Visible)
            {
                CenterTextBorder.Visibility = Visibility.Collapsed;
                LinkToOriginal.Visibility = Visibility.Collapsed;
                AdRotatorBlock.Visibility = Visibility.Collapsed;
                if (t < 1)
                {
                    t = 200;
                    Score.Text = score.ToString();
                    Next();
                }
                timer.Start();
            }
        }

        private void LinkToOriginal_Click(object sender, RoutedEventArgs e)
        {
            WebBrowserTask wbt = new WebBrowserTask();
            wbt.Uri = new Uri("http://anothergame.ru/pressxtowin/", UriKind.RelativeOrAbsolute);
            wbt.Show();
        }
        
        private void PhoneApplicationPage_BackKeyPress(object sender, CancelEventArgs e)
        {
            if (timer.IsEnabled)
            {
                timer.Stop();
                CenterTextBlock.Text = "PAUSE!\ncontinue?";
                CenterTextBorder.Visibility = Visibility.Visible;
                AdRotatorBlock.Visibility = Visibility.Visible;
                e.Cancel = true;
            }
            //setting["Theme"] = Theme.SelectedIndex;
            //setting.Save();
        }

        private void PhoneApplicationPage_OrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            if ((e.Orientation & PageOrientation.Portrait) == (PageOrientation.Portrait))
            {
                mX = 8;
                mY = 11;
                Max = mX * mY;
                CorrectImg.SetValue(Grid.ColumnProperty, 0);
                Title.SetValue(Grid.ColumnProperty, 0);
                Title.SetValue(Grid.ColumnSpanProperty, 3);
                TimeTitle.SetValue(Grid.ColumnProperty, 0);
                TimeTitle.SetValue(Grid.RowProperty, 1);
                TimeTitle.HorizontalAlignment = HorizontalAlignment.Left;
                ScoreTitle.SetValue(Grid.ColumnProperty, 2);
                ScoreTitle.SetValue(Grid.RowProperty, 1);
                ScoreTitle.HorizontalAlignment = HorizontalAlignment.Right;

                Time.SetValue(Grid.ColumnProperty, 0);
                Time.SetValue(Grid.RowProperty, 0);
                Timer.SetValue(Grid.ColumnProperty, 1);
                Timer.SetValue(Grid.RowProperty, 0);

                Score.SetValue(Grid.ColumnProperty, 0);
                Score.SetValue(Grid.RowProperty, 0);
                BestBorder.SetValue(Grid.ColumnProperty, 1);
                BestBorder.SetValue(Grid.RowProperty, 0);

                Theme.SetValue(Grid.ColumnProperty, 1);
                Theme.SetValue(Grid.RowProperty, 0);

                Panel.RowDefinitions.Clear();
                Panel.ColumnDefinitions.Clear();
                for (int i = 0; i < mY; i++)
                {
                    RowDefinition rd = new RowDefinition();
                    rd.Height = new GridLength(1, GridUnitType.Star);
                    Panel.RowDefinitions.Add(rd);
                }
                for (int i = 0; i < mX; i++)
                {
                    ColumnDefinition cd = new ColumnDefinition();
                    cd.Width = new GridLength(1, GridUnitType.Star);
                    Panel.ColumnDefinitions.Add(cd);
                }
                UpdatePos();
            }
            else
            {
                mX = 14;
                mY = 6;
                Max = mX * mY;
                if (count > Max) count = Max;
                CorrectImg.SetValue(Grid.ColumnProperty, 1);
                Title.SetValue(Grid.ColumnProperty, 1);
                Title.SetValue(Grid.ColumnSpanProperty, 1);
                TimeTitle.SetValue(Grid.ColumnProperty, 0);
                TimeTitle.SetValue(Grid.RowProperty, 0);
                TimeTitle.HorizontalAlignment = HorizontalAlignment.Center;
                ScoreTitle.SetValue(Grid.ColumnProperty, 2);
                ScoreTitle.SetValue(Grid.RowProperty, 0);
                ScoreTitle.HorizontalAlignment = HorizontalAlignment.Center;

                Time.SetValue(Grid.ColumnProperty, 0);
                Time.SetValue(Grid.RowProperty, 0);
                Timer.SetValue(Grid.ColumnProperty, 0);
                Timer.SetValue(Grid.RowProperty, 1);

                Score.SetValue(Grid.ColumnProperty, 0);
                Score.SetValue(Grid.RowProperty, 1);
                BestBorder.SetValue(Grid.ColumnProperty, 0);
                BestBorder.SetValue(Grid.RowProperty, 0);

                Theme.SetValue(Grid.ColumnProperty, 0);
                Theme.SetValue(Grid.RowProperty, 1);

                Panel.RowDefinitions.Clear();
                Panel.ColumnDefinitions.Clear();
                for (int i = 0; i < mY; i++)
                {
                    RowDefinition rd = new RowDefinition();
                    rd.Height = new GridLength(1, GridUnitType.Star);
                    Panel.RowDefinitions.Add(rd);
                }
                for (int i = 0; i < mX; i++)
                {
                    ColumnDefinition cd = new ColumnDefinition();
                    cd.Width = new GridLength(1, GridUnitType.Star);
                    Panel.ColumnDefinitions.Add(cd);
                }
                UpdatePos();
            }
        }
    }
}